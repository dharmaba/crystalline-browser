package user;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

public class CrystallineController implements Initializable {

    @FXML
    private WebView page;
    @FXML
    private Button prevBtn;
    @FXML
    private Button nextBtn;
    @FXML
    private Button searchBtn;
    @FXML
    private TextField searchBar;

    protected WebEngine currentPage;
    //Arraylist of all searches
    protected ArrayList<String> links = new ArrayList <> ();
    //Used to store the current place in the arraylist
    protected int currentItem = 0;

    @FXML
    private void search (ActionEvent e) {
        //Re-writes search history based on where a search is made in the links arraylist
        if ((currentItem-1) != (links.size()-1)) {
            int linkSize = links.size()-1;
            for (int i=linkSize; i >= currentItem; i--) {
                links.remove(i);
            }
            currentItem = links.size();
        }

        //If a search does not start with http:// or https://, http:// is added to it
        if (searchBar.getText().startsWith("http://") || searchBar.getText().startsWith("https://")) {
            links.add (searchBar.getText());
        } else {
            links.add ("http://"+searchBar.getText());
        }

        currentItem++;
        currentPage.load ((links.get(currentItem-1)));
        searchBar.setText ((links.get(currentItem-1)));
    }

    @FXML
    private void prevItem (ActionEvent e) {
        //Only go back if the arraylist and current index are large enough
        if (links.size() >= 2 && currentItem >= 2) {
            currentItem--;
            currentPage.load ((links.get(currentItem-1)));
            searchBar.setText ((links.get(currentItem-1)));
        }
    }

    @FXML
    private void nextItem (ActionEvent e) {
        //Only go forward if the current index is lesser than the arraylist's size
        if (currentItem <= (links.size()-1)) {
            currentItem++;
            currentPage.load ((links.get(currentItem-1)));
            searchBar.setText ((links.get(currentItem-1)));
        }
    }

    @Override
    public void initialize (URL url, ResourceBundle rb) {
        currentPage = page.getEngine ();
        URL firstPage = this.getClass().getResource("index.html");
        //Load the local start page first
        currentPage.load (firstPage.toString());
    }
}
