package user;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Crystalline extends Application {

    @Override
    public void start (Stage stage) throws Exception {
        Parent root = FXMLLoader.load (getClass().getResource ("Crystalline.fxml"));

        Scene scene = new Scene (root);

        stage.setScene (scene);
        //Set custom window title
        stage.setTitle ("Crystalline Browser");
        //Set custom window icon from file
        stage.getIcons ().add (new Image (CrystallineController.class.getResourceAsStream ("logo.png")));
        stage.show ();
    }
}
